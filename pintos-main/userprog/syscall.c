#include "userprog/syscall.h"
#include "filesys/file.h"
#include "filesys/filesys.h"
#include "userprog/pagedir.h"
#include "threads/interrupt.h"
#include "threads/vaddr.h"
#include "threads/malloc.h"
#include "devices/input.h"
#include "devices/shutdown.h"
#include <stdio.h>
#include <syscall-nr.h>
#include "vm/page.h"



void syscall_init(void) 
{
  intr_register_int(0x30, 3, INTR_ON, syscall_handler, "syscall");
  lock_init (&fs_lock);
  list_init (&open_files); 
  sema_init(&fs_sem, 1); 
}
struct list filesOpen;
struct file_descriptor
{
  int fd_num;
  tid_t owner;
  struct file *file_struct;
  struct list_elem elemList;
};

static void
syscall_handler (struct intr_frame *flNode)
{
  esp = flNode->esp;

  if (!is_valid_ptr (esp) || !is_valid_ptr (esp + 1) || !is_valid_ptr (esp + 2) || !is_valid_ptr (esp + 3))
    {
      exit (-1);
    }
    
    else
    {
      int syscall_number = *esp;
      switch (syscall_number)
      {
        
        case SYS_FILESIZE:
	          flNode->eax = filesize (*(esp + 1));  break;

        case SYS_EXEC:
          flNode->eax = exec ((char *) *(esp + 1));  break;
          

        case SYS_WAIT:
          flNode->eax = wait (*(esp + 1));  break;
         

        case SYS_CREATE:
          flNode->eax = create ((char *) *(esp + 1), *(esp + 2));  break;  
          

        case SYS_REMOVE:
          flNode->eax = remove ((char *) *(esp + 1));  break;
          

        case SYS_OPEN:
          flNode->eax = open ((char *) *(esp + 1));  break;
          

        case SYS_READ:
          flNode->eax = read (*(esp + 1), (void *) *(esp + 2), *(esp + 3));  break;
          

        case SYS_WRITE:
          flNode->eax = write (*(esp + 1), (void *) *(esp + 2), *(esp + 3)); break;
          
        case SYS_TELL:
          flNode->eax = tell (*(esp + 1));  break;

        case SYS_HALT:
          halt ();  break;
          
        case SYS_SEEK:
          seek (*(esp + 1), *(esp + 2));  break;

        case SYS_CLOSE:
          close (*(esp + 1));  break;

        case SYS_EXIT:
          exit (*(esp + 1));  break;
          
	      case SYS_MMAP:
	        flNode->eax = mmap (*(esp + 1), (void *) *(esp + 2)); break;
	       
	      case SYS_MUNMAP:
	        munmap (*(esp + 1)); break;

        default:
          break;
        }
    }
}

                                      // ***The system call functions***

    // ID assigned to threads for processs within kernel theads, both pid and tid use one to one mapping.
pid_t exec (const char *cmd_line)
{

  tid_t tid;
  struct thread *cur;

  if (!is_valid_ptr (cmd_line)) 
    {
      exit (-1);
    }

  cur = thread_current ();
  cur->child_load_status = 0;
  tid = process_execute (cmd_line);
  lock_acquire(&cur->lock_child);
  while (cur->child_load_status == 0)
    cond_wait(&cur->cond_child, &cur->lock_child);
  if (cur->child_load_status == -1)
    tid = -1;
  lock_release(&cur->lock_child);
  return tid;
}

int wait (pid_t pid)
{ 
  return process_wait(pid);
}

bool create (const char * Flname, unsigned size)
{
  bool pos;

  if (!is_valid_ptr ( Flname))
  {
    exit (-1);
  lock_acquire (&fs_lock);
  currStat = filesys_create( Flname, size);  
  lock_release (&fs_lock);
  }
  return pos;
}

bool remove (const char * Flname)
{
  bool currStat ;
  if (!is_valid_ptr ( Flname))
    exit (-1);

  lock_acquire (&fs_lock);  
  currStat = filesys_remove ( Flname);
  lock_release (&fs_lock);
  return currStat;
}

int open (const char * Flname)
{
  struct file *flNode;
  struct file_descriptor *flDesc;
  int currStat = -1;
  
  if (!is_valid_ptr ( Flname))
    exit (-1);

  lock_acquire (&fs_lock); 
 
  flNode = filesys_open ( Flname);
  if (flNode != NULL)
    {
      flDesc = calloc (1, sizeof *flDesc);
      flDesc->fd_num = allocate_fd ();
      flDesc->owner = thread_current ()->tid;
      flDesc->file_struct = flNode;
      list_push_back (&open_files, &flDesc->elemList);
      currStat = flDesc->fd_num;
    }
  lock_release (&fs_lock);
  return currStat;
}

int filesize (int flDesc)
{
  struct file_descriptor *fldescStruct;
  int currStat = -1;
  lock_acquire (&fs_lock); 
  fldescStruct = get_open_file (flDesc);
  if (fldescStruct != NULL)
    currStat = file_length (fldescStruct->file_struct);
  lock_release (&fs_lock);
  return currStat;
}

int read (int flDesc, void *buffer, unsigned size)  //To read into the buffer
{
  struct file_descriptor *flDescStruct;
  int currStat = 0;
  struct thread *t = thread_current ();
  unsigned sizeBuffer = size;
  void * tempBuffer = buffer;

  while (tempBuffer != NULL)  // to check of the memeory in the buffer is valid
    {
      if (!is_valid_uvaddr (tempBuffer))
	        exit (-1);

      if (pagedir_get_page (t->pagedir, tempBuffer) == NULL)   
	{ 
	  struct suppl_pte *spte;
	  spte = get_suppl_pte (&t->suppl_page_table, 
				pg_round_down (tempBuffer));

	        if (spte != NULL && !spte->is_loaded)
	            load_page (spte);

          else if (spte == NULL && tempBuffer >= (esp - 32))
	            grow_stack (tempBuffer);

	        else
	            exit (-1);
	}
      if (sizeBuffer == 0)
	{
	  tempBuffer = NULL;
	}

      else if (sizeBuffer > PGSIZE)
	{
	  tempBuffer += PGSIZE;
	  sizeBuffer -= PGSIZE;
	}

      else
	{
	  tempBuffer = buffer + size - 1;
	  sizeBuffer = 0;
	}
    }

  lock_acquire (&fs_lock);   
  if (flDesc == STDOUT_FILENO)
      currStat = -1;
  else if (flDesc == STDIN_FILENO)
    {
      uint8_t c;
      unsigned counter = size;
      uint8_t *buf = buffer;
      while (counter > 1 && (c = input_getc()) != 0)
        {
          *buf = c;
          buffer++;
          counter--; 
        }
      *buf = 0;
      currStat = size - counter;
    }
  else 
    {
      fldescStruct = get_open_file (flDesc);
      if (fldescStruct != NULL)
	currStat = file_read (fldescStruct->file_struct, buffer, size);
    }
  lock_release (&fs_lock);
  return currStat;
}

int write (int flDesc, const void *buffer, unsigned size)   //To write into the buffer
{
  struct file_descriptor *fldescStruct;  
  int currStat = 0;

  unsigned sizeBuffer = size;
  void *tempBuffer = buffer;

  while (tempBuffer != NULL)  // To check if the buffer memor is valid or not
    {
      if (!is_valid_ptr (tempBuffer))
	exit (-1);
      
      if (sizeBuffer > PGSIZE)
	      {
	      tempBuffer += PGSIZE;
	      sizeBuffer -= PGSIZE;
	    }

        else if (sizeBuffer == 0)
        {
	         tempBuffer = NULL;
	    }

      else
	{
    
	  tempBuffer = buffer + size - 1;
	  sizeBuffer = 0;
	}
}
  lock_acquire (&fs_lock); 
 
  if (flDesc == STDIN_FILENO)
    {
      currStat = -1;
    }
      else if (flDesc == STDOUT_FILENO)
      {
      putbuf (buffer, size);;
      currStat = size;
    }
      else 
    {
      fldescStruct = get_open_file (flDesc);
      if (fldescStruct != NULL)
	    currStat = file_write (fldescStruct->file_struct, buffer, size);
  }
  lock_release (&fs_lock);

  return currStat;
}
void close (int flDesc)
{
  struct file_descriptor *fldescStruct;
  lock_acquire (&fs_lock); 
  fldescStruct = get_open_file (flDesc);
  if (fldescStruct != NULL && fldescStruct->owner == thread_current ()->tid)
    close_open_file (flDesc);
  lock_release (&fs_lock);
  return ; 
}

void exit (int currStat)  //To exit the current thread
{
  struct ChildStat *child;
  struct thread *cur = thread_current ();
  printf ("%s: exit(%d)\n", cur->name, currStat);
  struct thread *parent = thread_get_by_id (cur->parent_id);
  if (parent != NULL) 
    {
      struct list_elem *elemnt = list_tail(&parent->children);
      while ((elemnt = list_prev (elemnt)) != list_head (&parent->children))
        {
          child = list_entry (elemnt, struct ChildStat, elem_child_status);
          if (child->child_id == cur->tid)
          {
            lock_acquire (&parent->lock_child);
            child->is_exit_called = true;
            child->child_exit_status = currStat;
            lock_release (&parent->lock_child);
          }
        }
    }
  thread_exit ();
}

void seek (int flDesc, unsigned pos)
{
  struct file_descriptor *fldescStruct;
  lock_acquire (&fs_lock); 
  fldescStruct = get_open_file (flDesc);
  if (fldescStruct != NULL)
    file_seek (fldescStruct->file_struct, pos);
  lock_release (&fs_lock);
  return ;
}

unsigned tell (int flDesc)
{
  struct file_descriptor *fldescStruct;
  int currStat = 0;
  lock_acquire (&fs_lock); 
  fldescStruct = get_open_file (flDesc);
  if (fldescStruct != NULL)
    currStat = file_tell (fldescStruct->file_struct);
  lock_release (&fs_lock);
  return currStat;
}

void halt (void)
{
  shutdown_power_off ();
}


mapid_t mmap (int flDesc, void *addr)
{
  struct file_descriptor *fldescStruct;
  int32_t len;
  struct thread *t = thread_current ();
  int offset;

  if (addr == NULL || addr == 0x0 || (pg_ofs (addr) != 0)) // To check the conditions and reject or accpet the coming requests 
    return -1;

  if(flDesc == 0 || flDesc == 1)
    return -1;
  fldescStruct = get_open_file (flDesc);
  if (fldescStruct == NULL)
    return -1;
  
  len = file_length (fldescStruct->file_struct);
  if (len <= 0)
    return -1;

  offset = 0;  // To check if there is sufficient space is available to execute the file from the uvaddr addr
  while (len  > offset)
    {
      if (get_suppl_pte (&t->suppl_page_table, addr + offset))
      {
	      return -1;
      }

      if (pagedir_get_page (t->pagedir, addr + offset))
      {
	        return -1;
      }
      offset += PGSIZE;
    }

  /* Add an entry in memory mapped files table, and add entries in
     supplemental page table iteratively which is in mmfiles_insert's
     semantic.
     If success, it will return the mapid;
     otherwise, return -1 */

  lock_acquire (&fs_lock);
  lock_release (&fs_lock);
  struct file* newfile = file_reopen(fldescStruct->file_struct);

  return (newfile == NULL) ? -1 : mmfiles_insert (addr, newfile, len);
}

void munmap (mapid_t mapping)
{
  /* Remove the entry in memory mapped files table, and remove corresponding
     entries in supplemental page table iteratively which is in 
     mmfiles_remove()'s semantic. */
  mmfiles_remove (mapping);
}

struct file_descriptor *
get_open_file (int flDesc)
{
  struct list_elem *elemnt;
  struct file_descriptor *fldescStruct; 
  elemnt = list_tail (&open_files);
  while ((elemnt = list_prev (elemnt)) != list_head (&open_files)) 
    {
      fldescStruct = list_entry (elemnt, struct file_descriptor, elemList);
      if (fldescStruct->fd_num == flDesc)
	return fldescStruct;
    }
  return NULL;
}

void close_open_file (int flDesc)  
{
  struct list_elem *elemnt;
  struct list_elem *prev;
  struct file_descriptor *fldescStruct; 
  elemnt = list_end (&open_files);
  while (elemnt != list_head (&open_files)) 
    {
      prev = list_prev (elemnt);
      fldescStruct = list_entry (elemnt, struct file_descriptor, elemList);
      if (fldescStruct->fd_num == flDesc)
	{
	  list_remove (elemnt);
          file_close (fldescStruct->file_struct);
	  free (fldescStruct);
	  return ;
	}
      elemnt = prev;
    }
  return ;
}
/* The kernel must be very careful about doing so, because the user can
 * pass a null pointer, a pointer to unmapped virtual memory, or a pointer
 * to kernel virtual address space (above PHYS_BASE). All of these types of
 * invalid pointers must be rejected without harm to the kernel or other
 * running processes, by terminating the offending process and freeing
 * its resources.
 */
bool is_valid_ptr (const void *usr_ptr)
{
  struct thread *cur = thread_current ();
  if (is_valid_uvaddr (usr_ptr))
    {
      return (pagedir_get_page (cur->pagedir, usr_ptr)) != NULL;
    }
  return false;
}

static bool is_valid_uvaddr (const void *uvaddr)
{
  return (uvaddr != NULL && is_user_vaddr (uvaddr));
}

int
allocate_fd ()
{
  static int fd_current = 1;
  return ++fd_current;
}

void close_file_by_owner (tid_t tid)
{
  struct list_elem *elemnt;
  struct list_elem *next;
  struct file_descriptor *fldescStruct; 
  elemnt = list_begin (&open_files);
  while (elemnt != list_tail (&open_files)) 
    {
      next = list_next (elemnt);
      fldescStruct = list_entry (elemnt, struct file_descriptor, elemList);
      if (fldescStruct->owner == tid)
	    {
	  list_remove (elemnt);
	  file_close (fldescStruct->file_struct);
          free (fldescStruct);
    	}
        elemnt = next;
    }
}
