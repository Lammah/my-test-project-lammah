#ifndef USERPROG_SYSCALL_H
#define USERPROG_SYSCALL_H
#include <stdio.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "filesys/file.h"
#include "filesys/filesys.h"
#include "lib/user/syscall.h"
#include <syscall-nr.h>
#include "threads/malloc.h"
#include "threads/synch.h"
#include "devices/shutdown.h"
#include "threads/vaddr.h"
#include "devices/input.h"
#include "userprog/process.h"

#define SYSCALL_VARIANT 0
#define arg1 4
#define arg2 8
#define arg3 12
#define ERROR_EXIT -1
#define filedsc 2

static struct semaphore fs_sem;  // Using semaphore to reduce the threads used together to 1.
struct filemap_t 
{struct list_elem ptr;
  int filedsc;
  struct file *file;};
void syscallInit(void);
static void syscall_handler(struct intr_frame *);      // Argument offsets for syscalls 
int systemWrite(int fd, const void *buffer, unsigned int length);
void systemExit(int status);

int systemFZ(int fd);
// Opens a file with the given filename and returns a file descriptor to it.
int systemOpen(const char *filename);

int systemRead(int fd, void *buffer, unsigned int length);
int   (pid_t pid);

void systemHalt(void);  // Terminates Pintos
bool systemCreate(const char *name, unsigned int size);
unsigned systemTell(int fd);
pid_t systemExec(const char *invocation);
bool systemRMV(const char *filename);
void systemClose(int fd);
void closeAllFl(struct thread *thread);
void systemSeek(int fd, unsigned pos);

static uint32_t loadPara(struct intr_frame *frame, int offset);
static int getUSR(const uint8_t *uaddr);
static bool putUSR(uint8_t *udst, uint8_t byte);
struct filemap_t *findFlmp(int fd);


#endif /* userprog/syscall.h */
